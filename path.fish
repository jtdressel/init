#!/usr/bin/env fish

#Add paths, if they are missing

set PATHS_TO_ADD ~/bin

for path in $PATHS_TO_ADD; 
    contains $path $fish_user_paths; or set -Ua fish_user_paths $path
end
