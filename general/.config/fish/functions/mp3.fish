# Defined in - @ line 1
function mp3 
  docker run --rm -it -v $PWD:/download jtdressel/youtube-dl:latest --add-metadata  --ignore-errors  --embed-thumbnail --extract-audio --audio-quality 0 --audio-format mp3 --newline --restrict-filenames --output '%(uploader)s-%(title)s-%(id)s.%(ext)s' --exec 'mv {} /download/' $argv;
end
