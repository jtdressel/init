#!/bin/bash
set -e
set -u
set -o pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# $DIR will always point to the directory containing this file. I can run
# this script from anywhere

stow --dotfiles --restow --target="$HOME" --dir="$DIR" general

