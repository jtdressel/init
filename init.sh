#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

declare -a MAIN_KEYS=(
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICiBzN/Hr8JLDsu0inUUHH5wtVaT1xs+HP0pGUqNyCyL opo-2 https://start.1password.com/open/i?a=5B7QMNMVTNBMHIDBFN7UWLT7VU&v=bkunpd3jd5uowpkkwehldkyn3i&i=k52c4vmfkalumpi5swybnvtjdm&h=my.1password.com"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOM5hzZ1bNXvFbPrXPh16QnwAfDtGuNdhpa4hsM+1Jvw blossom only key https://start.1password.com/open/i?a=5B7QMNMVTNBMHIDBFN7UWLT7VU&v=bkunpd3jd5uowpkkwehldkyn3i&i=yxowxuallpjnnamrqxw2tiqmbi&h=my.1password.com"
    "sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBEPzVsGmSc+/xj9M/RuEmMoq376ib8wa5FCv4+SxSL+UgXZd+hGvsBfJWT2G4lIdEgLpbonNbFpxjw9VthcKC6QAAAAUaWRfZWNkc2Ffc2tAYmxpbmsuc2g= Passkey id_ecdsa_sk blink"
    "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBARJFZfg5ZEcn6u20pq3fwGHXVT5FJijSoxZ36r+0b/3dBcMq9smiHPKuiINHDCsDFLhaV4FLZhj8uQLPwW9dw4= rarity SE blink"
    "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN6eUxT7sssrCvdJni1M5+Tjf5yMtSeL0c9rPlvh8BmP/HqmuSNxRwFBAbF6UblAzYJTqmr+L0bpYiXGsI+Kafk= starlightGlimmer SE blink"
    "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJbYbPr6pZCzaQjiRGPGPK9S6eqxd41jEIu+O4Safm7nZgAOPhcJak/5hNjUNWI4vN8W3PGBkZPA6i4Hgqan0IU= winona SE blink"
    "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBOXT/JJnVLqgD8haXaLaNmvyLw9qtXquw1kHRPnCjl675S0BJEhUwUVpKr2D8onieU7MFKt5GKQ+aUoXId1piU= found-tiger blink"
    "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIpyMYTP6psSEc7cQQtHwZNUIeh0k3X+zZ/L0NpPmMZOQUOtD7rfcGRpZGT/YK6zDbRmtVp12LvJ0RwOOWJdd+o= avp-1"
)

mkdir -p "$HOME/.ssh"

for val in ${MAIN_KEYS[@]}; do
   grep $val "$HOME/.ssh/authorized_keys" || echo $val >> "$HOME/.ssh/authorized_keys"
done

chmod 644 "$HOME/.ssh/authorized_keys"
